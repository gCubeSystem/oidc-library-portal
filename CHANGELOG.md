This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "oidc-library-portal"

## [v1.4.0]
- New handling for missing refresh token and refresh token expired in OIDC token when refreshing it.
- Changed OIDC related exceptions: the cause exception can be an OpenIdConnectRESTHelperException only.
- Changed OIDC/UMA refreshing errors log to DEBUG to avoid logs flood (#27536)

## [v1.3.2] - 2023-04-18
- UserSitesToGroupsAndRolesMapper class is now more resilient to Groups that are not found in Liferay and for which the LiferayGroupManager returns -1 as its Id instead of exception throwing (#24831)

## [v1.3.1] - 2021-06-24
- The UmaJWTProvider has been removed from authroization-common-client and new provider from that library (AccessTokenProvider) is used to transport the access-token only to the client library

## [v1.3.0] - 2021-04-29
- Extracted the UMA issuing code for logged user from the Valve in the threadlocal-vars-cleaner project to be used also after the login process for UMA issue in the context, since the Valve has already finished its work at that moment. (#20591)

## [v1.2.0] - 2021-01-27
- New context roles mapper that not rely on User or Groups objects but in their ID as strings. New revised version of the cache proxy with mutex and session id used in place of session object, logs revised and scheduled log added that dumps cache contents. Marked as deprecated all utils methods for session objects get/set. (#20445)

## [v1.1.0]
- Added configuration for avatar URL (#19726)

## [v1.0.0]
- First release (#19143) (#19225) (#19226) (#19227) (#19228)
