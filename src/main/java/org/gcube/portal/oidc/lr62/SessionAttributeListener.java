package org.gcube.portal.oidc.lr62;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class SessionAttributeListener implements HttpSessionAttributeListener {

    protected static final Log log = LogFactoryUtil.getLog(SessionAttributeListener.class);

    public SessionAttributeListener() {
        if (log.isTraceEnabled()) {
            log.trace("Listener created");
        }
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (log.isTraceEnabled()) {
            log.trace(
                    "[" + event.getSession().getId() + "] Added '" + event.getName() + "' in " + event.getSource()
                            + " with value: " + event.getValue());
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        if (log.isTraceEnabled()) {
            log.trace("[" + event.getSession().getId() + "] Removed '" + event.getName() + "' in " + event.getSource()
                    + " with value: " + event.getValue());
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        if (log.isTraceEnabled()) {
            log.trace("[" + event.getSession().getId() + "] Replaced '" + event.getName() + "' in " + event.getSource()
                    + " with value: " + event.getValue());
        }
    }

}
