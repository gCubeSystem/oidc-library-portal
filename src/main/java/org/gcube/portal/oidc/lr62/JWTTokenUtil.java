package org.gcube.portal.oidc.lr62;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gcube.oidc.rest.JWTToken;

public class JWTTokenUtil {

    public static String OIDC_TOKEN_ATTRIBUTE = "OIDC_JWT";
    public static String RPT_TOKEN_ATTRIBUTE = "UMA_RPT_JWT";

    public static JWTToken fromString(String tokenString) {
        return JWTToken.fromString(tokenString);
    }

    public static String getRawContent(JWTToken token) {
        return token.getRaw();
    }

    public static String getAccessTokenString(JWTToken token) {
        return token.getAccessTokenString();
    }

    public static JWTToken getOIDCFromRequest(HttpServletRequest request) {
        return JWTToken.fromString((String) request.getAttribute(OIDC_TOKEN_ATTRIBUTE));
    }

    public static void putOIDCInRequest(JWTToken token, HttpServletRequest request) {
        request.setAttribute(OIDC_TOKEN_ATTRIBUTE, getRawContent(token));
    }

    @Deprecated
    public static JWTToken getOIDCFromSession(HttpSession session) {
        return JWTToken.fromString((String) session.getAttribute(OIDC_TOKEN_ATTRIBUTE));
    }

    @Deprecated
    public static JWTToken getUMAFromSession(HttpSession session) {
        return JWTToken.fromString((String) session.getAttribute(RPT_TOKEN_ATTRIBUTE));
    }

    @Deprecated
    public static void putOIDCInSession(JWTToken token, HttpSession session) {
        session.setAttribute(OIDC_TOKEN_ATTRIBUTE, getRawContent(token));
    }

    @Deprecated
    public static void putUMAInSession(JWTToken token, HttpSession session) {
        session.setAttribute(RPT_TOKEN_ATTRIBUTE, getRawContent(token));
    }

    @Deprecated
    public static void removeOIDCFromSession(HttpSession session) {
        session.removeAttribute(OIDC_TOKEN_ATTRIBUTE);
    }

    @Deprecated
    public static void removeUMAFromSession(HttpSession session) {
        session.removeAttribute(RPT_TOKEN_ATTRIBUTE);
    }

}
