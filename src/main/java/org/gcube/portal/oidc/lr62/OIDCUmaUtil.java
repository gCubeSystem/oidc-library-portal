package org.gcube.portal.oidc.lr62;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.oidc.rest.JWTToken;
import org.gcube.oidc.rest.OpenIdConnectConfiguration;
import org.gcube.oidc.rest.OpenIdConnectRESTHelper;
import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

public class OIDCUmaUtil {

    private static final Log log = LogFactoryUtil.getLog(OIDCUmaUtil.class);

    private static final boolean REFRESH_UMA_TOKEN = false;
    private static final String LOGOUT_URI = "/c/portal/logout";
    private static final boolean FORCE_LOGOUT_ON_INVALID_TOKEN = true;
    private static final boolean FORCE_LOGOUT_ON_MISSING_OIDC = true;
    private static final boolean FORCE_LOGOUT_ON_MISSING_OIDC_REFRESH_TOKEN = true;
    private static final boolean FORCE_LOGOUT_ON_OIDC_REFRESH_ERROR = true;
    private static final int MAX_AUTHORIZATION_RETRY_ATTEMPTS = 4;
    private static final int AUTHORIZATION_RETRY_ATTEMPTS_DELAY = 4000;

    public static void provideConfiguredPortalClientUMATokenInThreadLocal(String infraContext) {
        OpenIdConnectConfiguration configuration = LiferayOpenIdConnectConfiguration.getConfiguration();
        String clientId = configuration.getPortalClientId();
        String clientSecret = configuration.getPortalClientSecret();

        provideClientUMATokenInThreadLocal(clientId, clientSecret, configuration.getTokenURL(), infraContext);
    }

    public static void provideClientUMATokenInThreadLocal(String clientId, String clientSecret, URL tokenURL,
            String infraContext) {

        try {
            if (log.isDebugEnabled()) {
                log.debug("Getting client token from server");
            }
            JWTToken clientToken = OpenIdConnectRESTHelper.queryClientToken(clientId, clientSecret, tokenURL);
            provideClientUMATokenInThreadLocal(clientToken.getAccessTokenAsBearer(), tokenURL, infraContext);
        } catch (Exception e) {
            log.error("Cannot retrieve client OIDC token", e);
            return;
        }
    }

    public static void provideClientUMATokenInThreadLocal(String clientAuthorizationBearer, URL tokenURL,
            String infraContext) {

        String encodedContext;
        try {
            encodedContext = URLEncoder.encode(infraContext, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("Cannot URL encode context", e);
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("URL encoded context is: " + encodedContext);
        }
        try {
            if (log.isDebugEnabled()) {
                log.debug("Getting UMA token from server");
            }
            JWTToken umaToken = OpenIdConnectRESTHelper.queryUMAToken(tokenURL, clientAuthorizationBearer,
                    encodedContext, null);

            provideTokensInThreadlocal(umaToken);

        } catch (Exception e) {
            log.error("Cannot retrieve client UMA token", e);
            return;
        }
    }

    /**
     * Used to request a temporary UMA/RPT token to the OIDC server in a specific context for an user.
     * 
     * The OIDC access token in the {@link JWTCacheProxy} for the user in the current <code>session</code> will be used.
     * 
     * @param request the current HTTP request
     * @param userId the user's user id
     * @param scope the scope to issue the token for (e.g. "/gcube")
     * @return the temporary token in the requested context for the user
     * @throws UMAException if a generic error related to the UMA/RPT token issue occurs
     * @throws InvalidTokenException if the OIDC access token is become invalid for the user
     * @throws MissingTokenException if the OIDC token is missing for the user in the cache proxy
     * @throws MissingRefreshTokenException if the refresh token is not present in current OIDC token
     * @throws RefreshException if an error occurs refreshing the OIDC token that is expired
     * @throws NotAuthorizedException if the user is not authorized to access the requested context
     * @throws RefreshTokenExpiredException if the refresh token has been expired (Keycloak session is expired)
     */
    public static JWTToken getUMAToken(HttpServletRequest request, Long userId, String scope)
            throws UMAException, InvalidTokenException, MissingTokenException, MissingRefreshTokenException,
            RefreshException, NotAuthorizedException, RefreshTokenExpiredException {

        return getUMAToken(request, userId, scope, true);
    }

    private static JWTToken getUMAToken(HttpServletRequest request, Long userId, String scope, boolean temporary)
            throws UMAException, InvalidTokenException, MissingTokenException, RefreshException,
            NotAuthorizedException, RefreshTokenExpiredException, MissingRefreshTokenException {

        try {
            return getUMAToken(request, UserLocalServiceUtil.getUser(userId), scope, temporary);
        } catch (PortalException | SystemException e) {
            throw new UMAException("Cannot get liferay user from userId", e);
        }
    }

    /**
     * Used to request a temporary UMA/RPT token to the OIDC server in a specific context for an user.
     * 
     * The OIDC access token in the {@link JWTCacheProxy} for the user in the current <code>session</code> will be used.
     * 
     * @param request the current HTTP request
     * @param screenName the user's screen name
     * @param scope the scope to issue the token for (e.g. "/gcube")
     * @return the temporary token in the requested context for the user
     * @throws UMAException if a generic error related to the UMA/RPT token issue occurs
     * @throws MissingTokenException if the OIDC token is missing for the user in the cache proxy
     * @throws MissingRefreshTokenException if the refresh token is not present in current OIDC token
     * @throws InvalidTokenException if the OIDC access token is become invalid for the user
     * @throws RefreshException if an error occurs refreshing the OIDC token that is expired
     * @throws NotAuthorizedException if the user is not authorized to access the requested context
     * @throws RefreshTokenExpiredException if the refresh token has been expired (Keycloak session is expired)
     */
    public static JWTToken getUMAToken(HttpServletRequest request, String screenName, String scope)
            throws InvalidTokenException, MissingTokenException, MissingRefreshTokenException,
            RefreshException, NotAuthorizedException, UMAException, RefreshTokenExpiredException {

        return getUMAToken(request, screenName, scope, true);
    }

    private static JWTToken getUMAToken(HttpServletRequest request, String screenName, String scope, boolean temporary)
            throws InvalidTokenException, MissingTokenException, MissingRefreshTokenException,
            RefreshException, NotAuthorizedException, UMAException, RefreshTokenExpiredException {

        try {
            return getUMAToken(request,
                    UserLocalServiceUtil.getUserByScreenName(PortalUtil.getCompanyId(request), screenName), scope,
                    temporary);

        } catch (PortalException | SystemException e) {
            throw new UMAException("Cannot get liferay user from companyId and screenName", e);
        }
    }

    public static JWTToken getUMAToken(HttpServletRequest request, User user, String scope)
            throws InvalidTokenException, MissingTokenException, MissingRefreshTokenException,
            RefreshException, NotAuthorizedException, UMAException, RefreshTokenExpiredException {

        return getUMAToken(request, user, scope, true);
    }

    private static JWTToken getUMAToken(HttpServletRequest request, User user, String scope, boolean temporary)
            throws InvalidTokenException, MissingTokenException, MissingRefreshTokenException,
            RefreshException, NotAuthorizedException, UMAException, RefreshTokenExpiredException {

        return getUMAToken(request, user, request.getSession(false), scope, temporary);
    }

    //    public static JWTToken getUMAToken(HttpServletRequest request, User user, HttpSession session, String scope)
    //            throws InvalidTokenException, MissingTokenException, RefreshException, NotAuthorizedException,
    //            UMAException {
    //
    //        return getUMAToken(request, user, session, scope, true);
    //    }

    private static JWTToken getUMAToken(HttpServletRequest request, User user, HttpSession session, String scope,
            boolean temporary) throws InvalidTokenException, MissingTokenException, MissingRefreshTokenException,
            RefreshException, NotAuthorizedException, UMAException, RefreshTokenExpiredException {

        if (user == null) {
            log.error("User is null, cannot continue");
            return null;
        }
        if (log.isDebugEnabled()) {
            log.debug("Current user is: " + user.getScreenName() + " [" + user.getEmailAddress() + "]");
        }

        if (session == null) {
            if (log.isDebugEnabled()) {
                log.debug("Session is null, cannot continue");
            }
            throw new UMAException("Session is null");
        }
        String sessionId = session.getId();
        if (log.isDebugEnabled()) {
            log.debug("Current session ID is " + sessionId);
        }

        String urlEncodedScope = null;
        try {
            urlEncodedScope = URLEncoder.encode(scope, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // Almost impossible
            log.error("Cannot URL encode scope", e);
            return null;
        }
        if (log.isDebugEnabled()) {
            log.debug("URL encoded scope is: " + urlEncodedScope);
        }

        JWTToken umaToken = null;
        JWTCacheProxy jwtCacheProxy = JWTCacheProxy.getInstance();
        synchronized (jwtCacheProxy.getMutexFor(user)) {
            if (log.isTraceEnabled()) {
                log.trace("Getting UMA token for user " + user.getScreenName() + ", and session " + sessionId);
            }
            umaToken = jwtCacheProxy.getUMAToken(user, sessionId);
            if (umaToken != null && !umaToken.isExpired() && umaToken.getAud().contains(urlEncodedScope)) {
                if (log.isTraceEnabled()) {
                    log.trace("Current UMA token is OK " + umaToken.getTokenEssentials());
                }
            } else {
                if (umaToken != null && umaToken.getAud().contains(urlEncodedScope) && umaToken.isExpired()) {
                    if (REFRESH_UMA_TOKEN) {
                        if (log.isDebugEnabled()) {
                            log.debug("Suitable UMA token found but is expired, trying to refresh it " +
                                    umaToken.getTokenEssentials());
                        }

                        OpenIdConnectConfiguration configuration = LiferayOpenIdConnectConfiguration
                                .getConfiguration(request);
                        try {
                            umaToken = OpenIdConnectRESTHelper.refreshToken(configuration.getTokenURL(), umaToken);
                            if (log.isDebugEnabled()) {
                                log.debug("Got a refreshed UMA token " + umaToken.getTokenEssentials());

                                log.debug("Setting the refreshed UMA token in cache proxy for user "
                                        + user.getScreenName() + ", and session " + sessionId);
                            }

                            // This is independent from "temporary" setting since is a refresh of an expired token
                            // issued for the same context
                            jwtCacheProxy.setUMAToken(user, sessionId, umaToken);
                        } catch (OpenIdConnectRESTHelperException e) {
                            if (e.hasJSONPayload()) {
                                if (OpenIdConnectRESTHelper.isInvalidBearerTokenError(e.getResponseString())) {
                                    throw new InvalidTokenException("UMA token become invalid", e);
                                } else if (OpenIdConnectRESTHelper.isTokenNotActiveError(e.getResponseString())) {
                                    log.info("UMA token is no more active, get new one");
                                } else {
                                    log.error("Other unexpected UMA token refresh error", e);
                                }
                            } else {
                                log.warn("Refreshing UMA token on server for " + umaToken.getTokenEssentials() + ": "
                                        + e.getMessage());
                            }
                            umaToken = null;
                            if (log.isDebugEnabled()) {
                                log.debug(
                                        "Removing inactive UMA token from cache proxy if present for user "
                                                + user.getScreenName() + " and session " + sessionId);
                            }

                            jwtCacheProxy.removeUMAToken(user, sessionId);
                        }
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("Suitable UMA token found but it is expired."
                                    + "It will be replaced with new one according to settings "
                                    + umaToken.getTokenEssentials());
                        }

                        umaToken = null;
                        if (log.isDebugEnabled()) {
                            log.debug("Removing inactive UMA token from cache proxy if present for user "
                                    + user.getScreenName() + " and session " + sessionId);
                        }

                        jwtCacheProxy.removeUMAToken(user, sessionId);
                    }
                }
                if (umaToken == null || !umaToken.getAud().contains(urlEncodedScope)) {
                    boolean scopeIsChanged = false;
                    if (umaToken == null) {
                        if (log.isDebugEnabled()) {
                            log.debug("Getting new UMA token for scope " + urlEncodedScope);
                        }
                    } else if (!umaToken.getAud().contains(urlEncodedScope)) {
                        scopeIsChanged = true;
                        log.info("Getting new UMA token for scope " + urlEncodedScope
                                + " since it has been issued for another scope " + umaToken.getTokenEssentials());
                    }
                    if (log.isDebugEnabled()) {
                        log.debug(
                                "Getting OIDC token from cache proxy for user " + user.getScreenName() + " and session "
                                        + sessionId);
                    }

                    JWTToken authToken = jwtCacheProxy.getOIDCToken(user, sessionId);
                    if (authToken == null) {
                        throw new MissingTokenException("Token is null in proxy");
                    } else if (!authToken.hasRefreshToken()) {
                        throw new MissingRefreshTokenException("Missing refresh token in OIDC access token");
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("OIDC token is " + authToken.getTokenEssentials());
                        }
                    }
                    OpenIdConnectConfiguration configuration = LiferayOpenIdConnectConfiguration
                            .getConfiguration(request);

                    boolean isNotAuthorized = false;
                    int authorizationAttempts = 0;
                    do {
                        try {
                            if (isNotAuthorized || scopeIsChanged || authToken.isExpired()) {
                                if (isNotAuthorized) {
                                    log.info(
                                            "UMA token is not authorized with current OIDC token, "
                                                    + "refreshing it to be sure that new grants are present. "
                                                    + "[attempts: " + authorizationAttempts + "]");

                                    // Resetting the flag to be sure to have correct log message each loop
                                    isNotAuthorized = false;
                                } else if (authToken.isRefreshTokenExpired()) {
                                    throw new RefreshTokenExpiredException(
                                            "Refresh token expired at "
                                                    + authToken.getRefreshTokenExpAsDate().toString());
                                } else if (scopeIsChanged) {
                                    log.info(
                                            "Scope is changed, refreshing token to be sure that new grants are present");
                                } else if (authToken.isExpired()) {
                                    if (log.isDebugEnabled()) {
                                        log.debug("OIDC token is expired, trying to refresh it "
                                                + authToken.getTokenEssentials());
                                    }
                                }
                                try {
                                    authToken = OpenIdConnectRESTHelper.refreshToken(configuration.getTokenURL(),
                                            authToken);
                                } catch (OpenIdConnectRESTHelperException e) {
                                    throw new RefreshException("Refreshing OIDC token", e);
                                }
                                log.debug("Setting refreshed OIDC token in cache proxy");
                                jwtCacheProxy.setOIDCToken(user, sessionId, authToken);
                            }
                            log.info("Getting UMA token from OIDC endpoint for scope: " + urlEncodedScope);
                            umaToken = OpenIdConnectRESTHelper.queryUMAToken(configuration.getTokenURL(),
                                    authToken.getAccessTokenAsBearer(), urlEncodedScope, null);

                            if (log.isDebugEnabled()) {
                                log.debug("Got new UMA token " + umaToken.getTokenEssentials());
                            }
                        } catch (OpenIdConnectRESTHelperException e) {
                            if (e.hasJSONPayload()) {
                                if (OpenIdConnectRESTHelper.isInvalidBearerTokenError(e.getResponseString())) {
                                    throw new InvalidTokenException("OIDC token is become invalid", e);
                                } else if (OpenIdConnectRESTHelper
                                        .isAccessDeniedNotAuthorizedError(e.getResponseString())) {

                                    log.info("UMA token is" + (isNotAuthorized ? " still" : "")
                                            + " not authorized with actual OIDC token");

                                    isNotAuthorized = true;
                                    authorizationAttempts += 1;
                                    if (authorizationAttempts <= MAX_AUTHORIZATION_RETRY_ATTEMPTS) {
                                        if (log.isDebugEnabled()) {
                                            log.debug("Sleeping " + AUTHORIZATION_RETRY_ATTEMPTS_DELAY
                                                    + " ms and looping refreshing the OIDC");
                                        }
                                        try {
                                            Thread.sleep(AUTHORIZATION_RETRY_ATTEMPTS_DELAY);
                                        } catch (InterruptedException ie) {
                                            ie.printStackTrace();
                                        }
                                    } else {
                                        throw new NotAuthorizedException("OIDC token refresh attempts exhausted");
                                    }
                                }
                            } else {
                                throw new UMAException(e);
                            }
                        }
                    } while (isNotAuthorized);
                }

                if (!temporary) {
                    if (log.isDebugEnabled()) {
                        log.debug("Setting UMA token in cache proxy for user " + user.getScreenName() + " and session "
                                + sessionId);
                    }

                    jwtCacheProxy.setUMAToken(user, sessionId, umaToken);
                }
            }
        }
        return umaToken;
    }

    public static void checkUMATicketAndProvideInThreadLocal(HttpServletRequest request, HttpServletResponse response,
            User user, HttpSession session, String scope) {

        try {
            JWTToken umaToken = getUMAToken(request, user, session, scope, false);
            if (log.isTraceEnabled()) {
                log.trace("Current UMA token in use is: " + umaToken.getTokenEssentials());
            }

            provideTokensInThreadlocal(umaToken);
        } catch (MissingTokenException e) {
            if (FORCE_LOGOUT_ON_MISSING_OIDC) {
                if (log.isDebugEnabled()) {
                    log.debug("OIDC token is null in cache proxy, force redirecting to logout URI");
                }
                forceLogout(response);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("OIDC token is null in cache proxy, cannot continue");
                }
            }
            log.debug("Removing UMA ticket from proxy");
            JWTCacheProxy.getInstance().removeUMAToken(user, session.getId());
            return;
        } catch (MissingRefreshTokenException e) {
            if (FORCE_LOGOUT_ON_MISSING_OIDC_REFRESH_TOKEN) {
                if (log.isDebugEnabled()) {
                    log.debug("Force redirecting to logout URI: " + e.getMessage());
                }
                forceLogout(response);
            } else {
                log.info(e.getMessage());
            }
            log.debug("Removing OIDC and UMA ticket from proxy");
            String sessionId = session.getId();
            JWTCacheProxy jwtCacheProxy = JWTCacheProxy.getInstance();
            jwtCacheProxy.removeOIDCToken(user, sessionId);
            jwtCacheProxy.removeUMAToken(user, sessionId);
            return;
        } catch (RefreshTokenExpiredException e) {
            if (FORCE_LOGOUT_ON_OIDC_REFRESH_ERROR) {
                log.info("Force redirecting to logout URI for " + user.getScreenName() + ": " + e.getMessage());
                forceLogout(response);
            } else {
                log.info(e.getMessage());
            }
            log.debug("Removing OIDC and UMA ticket from proxy");
            String sessionId = session.getId();
            JWTCacheProxy jwtCacheProxy = JWTCacheProxy.getInstance();
            jwtCacheProxy.removeOIDCToken(user, sessionId);
            jwtCacheProxy.removeUMAToken(user, sessionId);
            return;
        } catch (RefreshException e) {
            if (FORCE_LOGOUT_ON_OIDC_REFRESH_ERROR) {
                if (log.isDebugEnabled()) {
                    log.debug(
                            "Cannot refresh OIDC token, force redirect for " + user.getScreenName() + " to logout URI: "
                                    + e.getCause().getMessage());
                }

                forceLogout(response);
            } else {
                log.warn("Error refreshing OIDC token: " + e.getCause().getMessage());
            }
            return;
        } catch (InvalidTokenException e) {
            if (FORCE_LOGOUT_ON_INVALID_TOKEN) {
                if (log.isDebugEnabled()) {
                    log.debug("OIDC token is become invalid, forcing redirect to logout URI");
                }
                forceLogout(response);
            } else {
                log.warn("OIDC token is become invalid, cannot continue");
            }
            return;
        } catch (NotAuthorizedException e) {
            log.error("Access to scope " + scope + " is not authorized for user: " + user.getScreenName(), e);
        } catch (UMAException e) {
            log.error("Getting UMA token from server", e);
        }

    }

    public static void provideTokensInThreadlocal(JWTToken umaTokens) {
        if (log.isDebugEnabled()) {
            log.debug("Setting UMA token with jti " + umaTokens.getJti() + " in AccessToken provider");
        }
        AccessTokenProvider.instance.set(JWTTokenUtil.getAccessTokenString(umaTokens));
    }

    protected static void forceLogout(HttpServletResponse response) {
        try {
            if (!response.isCommitted()) {
                response.sendRedirect(LOGOUT_URI);
            } else {
                log.warn("Cannot redirect to logout URI since the response is already commited");
            }
        } catch (IOException e) {
            log.error("Cannot redirect to logout URI: " + LOGOUT_URI, e);
        }
    }

}
