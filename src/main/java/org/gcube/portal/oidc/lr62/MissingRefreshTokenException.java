package org.gcube.portal.oidc.lr62;

public class MissingRefreshTokenException extends OIDCException {

    private static final long serialVersionUID = -8014801905944311680L;

    public MissingRefreshTokenException(String message) {
        super(message);
    }

}
