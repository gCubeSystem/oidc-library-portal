package org.gcube.portal.oidc.lr62;

import java.util.Timer;
import java.util.TimerTask;

import org.gcube.oidc.rest.JWTToken;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;

public abstract class JWTCacheProxy {

    protected static final Log log = LogFactoryUtil.getLog(JWTCacheProxy.class);

    protected static JWTCacheProxy instance;

    public JWTCacheProxy() {
        instance = this;
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                instance.dumpOnLog();
            }
        }, 0, 60 * 1000);
    }

    public static synchronized JWTCacheProxy getInstance() {
        return instance != null ? instance : new IntraVMJWTCacheProxy();
    }

    public abstract void setOIDCToken(User user, String sessionId, JWTToken token);

    public abstract void setUMAToken(User user, String sessionId, JWTToken token);

    public abstract JWTToken getOIDCToken(User user, String sessionId);

    public abstract JWTToken getUMAToken(User user, String sessionId);

    public abstract void removeOIDCToken(User user, String sessionId);

    public abstract void removeUMAToken(User user, String sessionId);

    public abstract void removeAllOIDCTokens(User user);

    public abstract void removeAllUMATokens(User user);

    public abstract void clearOIDCTokens();

    public abstract void clearUMATokens();

    public abstract void clearAllTokens();

    public abstract Object getMutexFor(User user);

    public abstract void dumpOnLog();

}