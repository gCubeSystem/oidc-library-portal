package org.gcube.portal.oidc.lr62;

import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;

public class MissingTokenException extends OIDCException {

    private static final long serialVersionUID = -3156396867055045014L;

    public MissingTokenException(String message) {
        super(message);
    }

    public MissingTokenException(OpenIdConnectRESTHelperException cause) {
        super(cause);
    }

    public MissingTokenException(String message, OpenIdConnectRESTHelperException cause) {
        super(message, cause);
    }

}
