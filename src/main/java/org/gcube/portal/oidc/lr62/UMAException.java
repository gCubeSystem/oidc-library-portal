package org.gcube.portal.oidc.lr62;

public class UMAException extends Exception {

    private static final long serialVersionUID = 9158301486748240970L;

    public UMAException(String message) {
        super(message);
    }

    public UMAException(Throwable cause) {
        super(cause);
    }

    public UMAException(String message, Throwable cause) {
        super(message, cause);
    }

}
