package org.gcube.portal.oidc.lr62;

import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;

public class InvalidTokenException extends OIDCException {

    private static final long serialVersionUID = 6104023137356404839L;

    public InvalidTokenException(String message) {
        super(message);
    }

    public InvalidTokenException(OpenIdConnectRESTHelperException cause) {
        super(cause);
    }

    public InvalidTokenException(String message, OpenIdConnectRESTHelperException cause) {
        super(message, cause);
    }

}
