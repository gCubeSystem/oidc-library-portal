package org.gcube.portal.oidc.lr62;

import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;

public class RefreshException extends OIDCException {

    private static final long serialVersionUID = -8657006296380516704L;

    public RefreshException(String message) {
        super(message);
    }

    public RefreshException(OpenIdConnectRESTHelperException cause) {
        super(cause);
    }

    public RefreshException(String message, OpenIdConnectRESTHelperException cause) {
        super(message, cause);
    }

}
