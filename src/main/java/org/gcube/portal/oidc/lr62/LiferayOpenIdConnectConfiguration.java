package org.gcube.portal.oidc.lr62;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.gcube.oidc.rest.OpenIdConnectConfiguration;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.util.PortalUtil;

public class LiferayOpenIdConnectConfiguration implements OpenIdConnectConfiguration {

    protected static final Log log = LogFactoryUtil.getLog(LiferayOpenIdConnectConfiguration.class);

    public static Map<Long, LiferayOpenIdConnectConfiguration> companyId2Configuration = Collections
            .synchronizedMap(new HashMap<Long, LiferayOpenIdConnectConfiguration>());

    private Long companyId;
    private URL authorizationURL;
    private URL tokenURL;
    private URL logoutURL;
    private URL issuerURL;
    private URL avatarURL;
    private String portalClientId;
    private String portalClientSecret;
    private String scope;
    private boolean logoutOnPortalLogout;
    private boolean createUnexistingUser;

    public static synchronized LiferayOpenIdConnectConfiguration getConfiguration(Long companyId) {
        log.trace("Getting config from companyId");
        if (!companyId2Configuration.containsKey(companyId)) {
            companyId2Configuration.put(companyId, new LiferayOpenIdConnectConfiguration(companyId));
        }
        return companyId2Configuration.get(companyId);
    }

    public static synchronized LiferayOpenIdConnectConfiguration getConfiguration(HttpServletRequest request) {
        log.trace("Getting config from request");
        return LiferayOpenIdConnectConfiguration.getConfiguration(PortalUtil.getCompanyId(request));
    }

    public static synchronized LiferayOpenIdConnectConfiguration getConfiguration() {
        log.trace("Getting config from thread local");
        return LiferayOpenIdConnectConfiguration.getConfiguration(CompanyThreadLocal.getCompanyId());
    }

    private LiferayOpenIdConnectConfiguration(Long companyId) {
        log.info("Creating config from companyId: " + companyId);
        this.companyId = companyId;
        try {
            this.authorizationURL = new URL(PrefsPropsUtil.getString(companyId, "d4science.oidc-authorization"));
            this.tokenURL = new URL(PrefsPropsUtil.getString(companyId, "d4science.oidc-token"));
            this.logoutURL = new URL(PrefsPropsUtil.getString(companyId, "d4science.oidc-logout"));
            this.issuerURL = new URL(PrefsPropsUtil.getString(companyId, "d4science.oidc-issuer"));
            this.avatarURL = new URL(PrefsPropsUtil.getString(companyId, "d4science.oidc-keycloak_avatar"));
            this.portalClientId = PrefsPropsUtil.getString(companyId, "d4science.oidc-portal-client-id");
            this.portalClientSecret = PrefsPropsUtil.getString(companyId, "d4science.oidc-portal-client-secret");
            this.scope = PrefsPropsUtil.getString(companyId, "d4science.oidc-scope");
            this.logoutOnPortalLogout = PrefsPropsUtil.getBoolean(companyId, "d4science.oidc-logout-on-portal-logout");
            this.createUnexistingUser = PrefsPropsUtil.getBoolean(companyId, "d4science.oidc-create-unexisting-user");
        } catch (SystemException | MalformedURLException e) {
            throw new RuntimeException(e);
        }
        log.debug("authorizationURL=" + getAuthorizationURL());
        log.debug("tokenURL=" + getTokenURL());
        log.debug("logoutURL=" + getLogoutURL());
        log.debug("issuerURL=" + getIssuerURL());
        log.debug("avatarURL=" + getAvatarURL());
        log.debug("portalClientId=" + getPortalClientId());
        log.debug("portalClientSecret=" + getPortalClientSecret().replaceAll("\\w", "*"));
        log.debug("scope=" + getScope());
        log.debug("logoutOnPortalLogout=" + logoutOnPortalLogout());
        log.debug("createUnexistingUser=" + createUnexistingUser());
    }

    public Long getCompanyId() {
        return companyId;
    }

    @Override
    public URL getAuthorizationURL() {
        return this.authorizationURL;
    }

    @Override
    public URL getTokenURL() {
        return this.tokenURL;
    }

    @Override
    public URL getLogoutURL() {
        return this.logoutURL;
    }

    @Override
    public URL getIssuerURL() {
        return this.issuerURL;
    }

    @Override
    public String getPortalClientId() {
        return this.portalClientId;
    }

    public String getPortalClientSecret() {
        return this.portalClientSecret;
    }

    @Override
    public String getScope() {
        return this.scope;
    }

    public boolean logoutOnPortalLogout() {
        return this.logoutOnPortalLogout;
    }

    public boolean createUnexistingUser() {
        return this.createUnexistingUser;
    }

    @Override
    public URL getAvatarURL() {
        return avatarURL;
    }

}
