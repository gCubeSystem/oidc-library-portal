package org.gcube.portal.oidc.lr62;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import org.gcube.oidc.rest.JWTToken;

import com.liferay.portal.model.User;

public class IntraVMJWTCacheProxy extends JWTCacheProxy {

    Map<User, Map<String, String>> oidcTokens;
    Map<User, Map<String, String>> rptTokens;

    private Comparator<User> userComparator = new Comparator<User>() {
        @Override
        public int compare(User u1, User u2) {
            return u1.getScreenName().compareTo(u2.getScreenName());
        }
    };

    protected IntraVMJWTCacheProxy() {
        super();
        log.info("Initializing tokens caches");
        oidcTokens = Collections.synchronizedMap(new TreeMap<>(userComparator));
        rptTokens = Collections.synchronizedMap(new TreeMap<>(userComparator));
    }

    protected void checkMap(Map<User, Map<String, String>> map, User user) {
        if (!map.containsKey(user)) {
            if (log.isDebugEnabled()) {
                log.debug("Initializing map for user: " + user.getScreenName());
            }
            map.put(user, Collections.synchronizedMap(new TreeMap<>()));
        }
    }

    @Override
    public void setOIDCToken(User user, String sessionId, JWTToken token) {
        checkMap(oidcTokens, user);
        Map<String, String> oidcUserMap = oidcTokens.get(user);
        if (log.isTraceEnabled()) {
            log.trace("Setting OIDC token for user '" + user.getScreenName() + "' and session: " + sessionId);
        }
        oidcUserMap.put(sessionId, JWTTokenUtil.getRawContent(token));
        if (log.isTraceEnabled()) {
            log.trace("OIDC user's sessions recorded objs: " + oidcUserMap.keySet().toArray());
        }
    }

    @Override
    public void setUMAToken(User user, String sessionId, JWTToken token) {
        checkMap(rptTokens, user);
        Map<String, String> umaUserMap = rptTokens.get(user);
        if (log.isTraceEnabled()) {
            log.trace("Setting UMA token for user '" + user.getScreenName() + "' and session: " + sessionId);
        }
        umaUserMap.put(sessionId, JWTTokenUtil.getRawContent(token));
        if (log.isTraceEnabled()) {
            log.trace("UMA user's sessions recorded objs: " + umaUserMap.keySet().toArray());
        }
    }

    @Override
    public JWTToken getOIDCToken(User user, String sessionId) {
        checkMap(oidcTokens, user);
        if (log.isTraceEnabled()) {
            log.trace("Getting OIDC token for user '" + user.getScreenName() + "' and session: " + sessionId);
        }
        return JWTTokenUtil.fromString(oidcTokens.get(user).get(sessionId));
    }

    @Override
    public JWTToken getUMAToken(User user, String sessionId) {
        checkMap(rptTokens, user);
        if (log.isTraceEnabled()) {
            log.trace("Getting UMA token for user '" + user.getScreenName() + "' and session: " + sessionId);
        }
        return JWTTokenUtil.fromString(rptTokens.get(user).get(sessionId));
    }

    @Override
    public void removeOIDCToken(User user, String sessionId) {
        checkMap(oidcTokens, user);
        if (oidcTokens.get(user).containsKey(sessionId)) {
            if (log.isTraceEnabled()) {
                log.trace("Removing OIDC token for user '" + user.getScreenName() + "' and session: " + sessionId);
            }
            oidcTokens.get(user).remove(sessionId);
        } else {
            log.info("No OIDC token is stored for user '" + user.getScreenName() + "' and session: " + sessionId);
        }
    }

    @Override
    public void removeUMAToken(User user, String sessionId) {
        checkMap(rptTokens, user);
        if (rptTokens.get(user).containsKey(sessionId)) {
            if (log.isTraceEnabled()) {
                log.trace("Removing UMA token for user '" + user.getScreenName() + "' and session: " + sessionId);
            }
            rptTokens.get(user).remove(sessionId);
        } else {
            if (log.isTraceEnabled()) {
                log.debug("No UMA token is stored for user '" + user.getScreenName() + "' and session: " + sessionId);
            }
        }
    }

    @Override
    public void removeAllOIDCTokens(User user) {
        if (log.isTraceEnabled()) {
            log.trace("Removing all OIDC session's tokens of user: " + user.getScreenName());
        }
        oidcTokens.remove(user);
    }

    @Override
    public void removeAllUMATokens(User user) {
        if (log.isTraceEnabled()) {
            log.trace("Removing all UMA session's tokens of user: " + user.getScreenName());
        }
        rptTokens.remove(user);
    }

    @Override
    public void clearOIDCTokens() {
        if (log.isTraceEnabled()) {
            log.trace("Clearing OIDC tokens cache");
        }
        oidcTokens.clear();
    }

    @Override
    public void clearUMATokens() {
        if (log.isTraceEnabled()) {
            log.trace("Clearing UMA tokens cache");
        }
        rptTokens.clear();
    }

    @Override
    public void clearAllTokens() {
        clearOIDCTokens();
        clearUMATokens();
    }

    @Override
    public synchronized Map<String, String> getMutexFor(User user) {
        checkMap(oidcTokens, user);
        return oidcTokens.get(user);
    }

    @Override
    public void dumpOnLog() {
        if (log.isTraceEnabled()) {
            log.trace("Cached OIDC objects: " + dumpMap(oidcTokens));
            log.trace("Cached UMA objects: " + dumpMap(rptTokens));
        }
    }

    protected String dumpMap(Map<User, Map<String, String>> map) {
        StringBuffer sb = new StringBuffer();
        map.forEach((u, v) -> {
            sb.append(u.getScreenName() + " :");
            v.forEach((s, t) -> {
                sb.append(" " + s + " -> " + JWTToken.fromString(t).getTokenEssentials());
            });
            sb.append(", ");
        });
        return sb.toString();
    }
}
