package org.gcube.portal.oidc.lr62;

import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;

public class OIDCException extends Exception {

    private static final long serialVersionUID = -882800971125567037L;

    public OIDCException(String message) {
        super(message);
    }

    public OIDCException(OpenIdConnectRESTHelperException cause) {
        super(cause);
    }

    public OIDCException(String message, OpenIdConnectRESTHelperException cause) {
        super(message, cause);
    }

    @Override
    public synchronized OpenIdConnectRESTHelperException getCause() {
        return (OpenIdConnectRESTHelperException) super.getCause();
    }

}
