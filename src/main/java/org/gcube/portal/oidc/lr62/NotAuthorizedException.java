package org.gcube.portal.oidc.lr62;

import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;

public class NotAuthorizedException extends OIDCException {

    private static final long serialVersionUID = 67891107946900764L;

    public NotAuthorizedException(String message) {
        super(message);
    }

    public NotAuthorizedException(OpenIdConnectRESTHelperException cause) {
        super(cause);
    }

    public NotAuthorizedException(String message, OpenIdConnectRESTHelperException cause) {
        super(message, cause);
    }

}
