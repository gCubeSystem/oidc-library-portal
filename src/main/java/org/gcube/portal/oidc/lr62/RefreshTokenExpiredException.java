package org.gcube.portal.oidc.lr62;

public class RefreshTokenExpiredException extends OIDCException {

    private static final long serialVersionUID = -8014801905944311680L;

    public RefreshTokenExpiredException(String message) {
        super(message);
    }

}
